<?php

namespace EveBlues\Model\Corporation;

use EveBlues\Model\Alliance\Alliance;
use EveBlues\Model\Character\Character;
use EveBlues\Model\Contact\CorporationContact;
use Nextras\Dbal\Utils\DateTime;
use Nextras\Orm\Entity\Entity;
use Nextras\Orm\Relationships\OneHasMany;

/**
 * Represents one Character
 *
 * @author Jakub Pistek <mail@jakubpistek.cz>
 *
 * @property int $id {primary}
 * @property string|null $name
 * @property Alliance|null $alliance {m:1 Alliance::$corporations}
 * @property DateTime|null $startDate
 * @property boolean|null $isNpc
 * @property boolean $isFinished
 *
 * @property OneHasMany|Character[] $characters {1:m Character::$corporation}
 * @property OneHasMany|CorporationContact[] $contacts {1:m CorporationContact::$owner}
 */
class Corporation extends Entity {

}
