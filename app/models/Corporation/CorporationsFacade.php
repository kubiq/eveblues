<?php

namespace EveBlues\Model\Corporation;

use EveBlues\Model\Contact\AllianceContactsRepository;
use EveBlues\Model\Contact\CharacterContactsRepository;
use EveBlues\Model\Contact\CorporationContactsRepository;
use Kdyby\Monolog\Logger;

/**
 * @author Jakub Pistek <mail@jakubpistek.cz>
 */
class CorporationsFacade {

	/** @var Logger */
	private $logger;
	/** @var CorporationsRepository */
	private $repository;
	/** @var CharacterContactsRepository */
	private $characterContactsRepository;
	/** @var CorporationContactsRepository */
	private $corporationContactsRepository;
	/** @var AllianceContactsRepository */
	private $allianceContactsRepository;

	public function __construct(
		CorporationsRepository $repository,
		CharacterContactsRepository $characterContactsRepository,
		CorporationContactsRepository $corporationContactsRepository,
		AllianceContactsRepository $allianceContactsRepository,
		Logger $logger
	) {
		$this->repository = $repository;
		$this->logger = $logger->channel('corporation');
		$this->characterContactsRepository = $characterContactsRepository;
		$this->corporationContactsRepository = $corporationContactsRepository;
		$this->allianceContactsRepository = $allianceContactsRepository;
	}

	/**
	 * Gets all corps which are in contacts but not in corp table
	 */
	public function getMissingCorporations() {

	}

}
