<?php

namespace EveBlues\Model\Corporation;

use EveBlues\Model\Alliance\Alliance;
use Nextras\Orm\Repository\Repository;

/**
 * @author Jakub Pistek <mail@jakubpistek.cz>
 */
class CorporationsRepository extends Repository {

	/**
	 * @return string[]
	 */
	public static function getEntityClassNames() {
		return [Corporation::class];
	}

	public function createMore(Alliance $alliance, $corporationsData) {

		$corporations = [];
		foreach ($corporationsData as $data) {
			$c = $this->create($alliance, $data);
			$this->persist($c);
			$corporations[$c->id] = $c;
		}
		$this->flush();

		return $corporations;
	}

	public function createNew($corporationData) :Corporation {
		$corporation = $this->getById($corporationData['id']);
		if (!$corporation) {
			$corporation = new Corporation();
			$corporation->id = $corporationData['id'];
			$corporation->name = $corporationData['name'];
			$corporation->isNpc = $corporationData['isNPC'];
			$corporation->isFinished = true;

			$this->persistAndFlush($corporation);
		}

		return $corporation;
	}

	/**
	 * Pro vytvareni z crest sso
	 * 
	 * @param int $id
	 * @param string $name
	 * @param null $alliance
	 * @return Corporation
	 */
	public function createNewStub(int $id, string $name, $alliance = null) :Corporation {
		$corporation = $this->getById($id);
		if (!$corporation) {
			$corporation = new Corporation();
			$corporation->id = $id;
			$corporation->name = $name;
			$corporation->isFinished = false;
			$corporation->alliance = $alliance;

			$this->persistAndFlush($corporation);
		}

		return $corporation;
	}

	private function create(Alliance $alliance, $corporationData) :Corporation {
		$corporation = $this->getById($corporationData['corporationID']);
		if (!$corporation) {
			$corporation = new Corporation();
			$corporation->id = $corporationData['corporationID'];
			$corporation->startDate = $corporationData['startDate'];
			$corporation->isFinished = false;
		}
		$corporation->alliance = $alliance;

		return $corporation;
	}
}
