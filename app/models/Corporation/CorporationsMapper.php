<?php

namespace EveBlues\Model\Corporation;

use Nextras\Orm\Mapper\Mapper;

/**
 * @author Jakub Pistek <mail@jakubpistek.cz>
 */
class CorporationsMapper extends Mapper {

	/** @var string */
	protected $tableName = 'corporations';

	public function findMissingCharacterCorps() {
		
	}
}
