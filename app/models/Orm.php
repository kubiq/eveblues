<?php

namespace EveBlues\Model;

use EveBlues\Model\Alliance\AlliancesRepository;
use EveBlues\Model\ApiKey\ApiKeysRepository;
use EveBlues\Model\Character\CharactersRepository;
use EveBlues\Model\Contact\AllianceContactsRepository;
use EveBlues\Model\Contact\CharacterContactsRepository;
use EveBlues\Model\Contact\ContactLabelsRepository;
use EveBlues\Model\Contact\CorporationContactsRepository;
use EveBlues\Model\Corporation\CorporationsRepository;
use EveBlues\Model\User\UsersRepository;
use Nextras\Orm\Model\Model;

/**
 *
 * @author Jakub Pistek <mail@jakubpistek.cz>
 *
 * @property-read AlliancesRepository $alliances
 * @property-read ApiKeysRepository $keys
 * @property-read CharactersRepository $characters
 * @property-read CorporationsRepository $corporations
 * @property-read CharacterContactsRepository $characterContacts
 * @property-read CorporationContactsRepository $corporationContacts
 * @property-read AllianceContactsRepository $allianceContacts
 * @property-read ContactLabelsRepository $contactLabels
 * @property-read UsersRepository $users
 */
class Orm extends Model {

}
