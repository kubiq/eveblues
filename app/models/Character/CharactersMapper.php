<?php

namespace EveBlues\Model\Character;

use Nextras\Orm\Mapper\Mapper;

/**
 * @author Jakub Pistek <mail@jakubpistek.cz>
 */
class CharactersMapper extends Mapper {

	/** @var string */
	protected $tableName = 'characters';

	/**
	 * @return \Nextras\Orm\Mapper\Dbal\StorageReflection\UnderscoredStorageReflection
	 */
	public function createStorageReflection() {

		$reflection = parent::createStorageReflection();
//		$reflection->addMapping('author', 'user_id');
//		$reflection->addMapping('externalsRaw', 'externals');

		return $reflection;
	}

	
}
