<?php

namespace EveBlues\Model\Character;

use EveBlues\Model\ApiKey\ApiKey;
use EveBlues\Model\Corporation\Corporation;
use EveBlues\Model\User\User;
use League\OAuth2\Client\Token\AccessToken;
use Nextras\Orm\Repository\Repository;

/**
 * @author Jakub Pistek <mail@jakubpistek.cz>
 */
class CharactersRepository extends Repository {

	/**
	 * @return string[]
	 */
	public static function getEntityClassNames() {
		return [Character::class];
	}

	/**
	 * @param User $owner
	 * @param int $id
	 * @param string $name
	 * @param Corporation $corp
	 * @param AccessToken $token
	 * @return Character
	 */
	public function createNewFromData(User $owner, int $id, string $name, Corporation $corp, AccessToken $token) :Character {
		$character = $this->getById($id);
		if (!$character) {
			$character = new Character();
			$character->id = $id;
			$character->name = $name;
			$character->owner = $owner;
			$character->isDownloaded = false;
		}

		$character->corporation = $corp;
		$character->isVisible = true;
		$character->token = $token->getToken();
		$character->tokenExpires = $token->getExpires();
		$character->hasToken = true;
		$this->persistAndFlush($character);

		return $character;
	}

	/**
	 * @param ApiKey $apiKey
	 * @param mixed $data
	 * @return Character
	 */
	public function createNewFromApiKey(ApiKey $apiKey, $data) :Character {
		$character = $this->getById($data['characterID']);
		if (!$character) {
			$character = new Character();
			$character->id = $data['characterID'];
			$character->name = $data['characterName'];
			$character->owner = $apiKey->owner;
			$character->isVisible = false;
			$character->hasToken = false;
			$character->isDownloaded = false;
		}

		if (!$apiKey->characters->has($character)) {
			$apiKey->characters->add($character);
		}

		$this->persistAndFlush($character);

		return $character;
	}

	/**
	 * @param int $id
	 * @param string $name
	 * @return Character
	 */
	public function createNewStub(int $id, string $name) :Character {
		$character = $this->getById($id);
		if (!$character) {
			$character = new Character();
			$character->id = $id;
			$character->name = $name;
			$character->isVisible = false;
			$character->hasToken = false;
			$character->isDownloaded = false;
		}

		$this->persistAndFlush($character);

		return $character;
	}

	/**
	 * @param int $id
	 * @param string $name
	 * @param Corporation|null $corporation
	 * @return Character
	 */
	public function createNew(int $id, string $name, Corporation $corporation = null) :Character {
		$character = $this->getById($id);
		if (!$character) {
			$character = new Character();
			$character->id = $id;
			$character->name = $name;
			$character->isVisible = false;
			$character->hasToken = false;
			$character->isDownloaded = false;
		}
		$character->corporation = $corporation;

		$this->persistAndFlush($character);

		return $character;
	}

	/**
	 * @param User $user
	 * @return Character[]
	 */
	public function findWithToken(User $user) {

		return $this->findBy([
			'owner' => $user,
			'hasToken' => true
		]);
	}

	/**
	 * @param User $user
	 * @return Character[]
	 */
	public function findDownloaded(User $user) {

		return $this->findBy([
			'owner' => $user,
			'isDownloaded' => true
		]);
	}

	/**
	 * @param User $user
	 * @return Character[]
	 */
	public function findByOwner(User $user) {

		return $this->findBy([
			'owner' => $user
		]);
	}

	/**
	 * @param int $limit
	 * @return Character[]
	 */
	public function findUnfinished(int $limit = 100) {

		return $this->findBy([
			'corporation' => null
		])
			->limitBy($limit);
	}

}
