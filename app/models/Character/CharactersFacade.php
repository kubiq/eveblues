<?php

namespace EveBlues\Model\Character;

use EveBlues\Model\Alliance\AlliancesRepository;
use EveBlues\Model\ApiKey\ApiKey;
use EveBlues\Model\ApiKey\ApiKeysRepository;
use EveBlues\Model\Corporation\CorporationsRepository;
use EveBlues\Model\User\User;
use EveBlues\Model\User\UsersRepository;
use EveBlues\XmlApi\XmlApi;
use Kdyby\Monolog\Logger;
use League\OAuth2\Client\Token\AccessToken;

/**
 * @author Jakub Pistek <mail@jakubpistek.cz>
 */
class CharactersFacade {

	/** @var Logger */
	private $logger;
	/** @var XmlApi */
	private $xmlApi;
	/** @var CharactersRepository */
	private $charactersRepository;
	/** @var CorporationsRepository */
	private $corporationsRepository;
	/** @var AlliancesRepository */
	private $alliancesRepository;
	/** @var UsersRepository */
	private $usersRepository;
	/** @var ApiKeysRepository */
	private $keysRepository;

	public function __construct(
		ApiKeysRepository $keysRepository,
		CharactersRepository $charactersRepository,
		CorporationsRepository $corporationsRepository,
		AlliancesRepository $alliancesRepository,
		UsersRepository $usersRepository,
		Logger $logger,
		XmlApi $xmlApi
	) {
		$this->logger = $logger->channel('character');
		$this->xmlApi = $xmlApi;
		$this->charactersRepository = $charactersRepository;
		$this->corporationsRepository = $corporationsRepository;
		$this->alliancesRepository = $alliancesRepository;
		$this->usersRepository = $usersRepository;
		$this->keysRepository = $keysRepository;
	}

	public function createNew(User $user, int $characterId, AccessToken $token) :Character {
		$characterData = $this->xmlApi->getCharacter($characterId);

		$alliance = null;
		if ($characterData['allianceID']) {
			$alliance = $this->alliancesRepository->getById($characterData['allianceID']);
			if (!$alliance) {
				$alliance = $this->alliancesRepository->createNewStub($characterData['allianceID'], $characterData['allianceName']);
			}
		}

		$corp = $this->corporationsRepository->getById($characterData['corporationID']);
		if (!$corp) {
			$corp = $this->corporationsRepository->createNewStub($characterData['corporationID'], $characterData['corporationName'], $alliance);
		}

		$character = $this->charactersRepository->getById($characterData['characterID']);
		if (!$character) {
			$character = $this->charactersRepository->createNewFromData($user, $characterData['characterID'], $characterData['characterName'], $corp, $token);
		}

		return $character;
	}

	/**
	 * Saves new character its corp and alliance
	 *
	 * @param ApiKey $apiKey
	 * @param array $characterData
	 * @return Character
	 */
	public function createNewFromApiKey(ApiKey $apiKey, array $characterData) :Character {

		$alliance = $this->alliancesRepository->createNewStub($characterData['allianceID'], $characterData['allianceName']);
		$corp = $this->corporationsRepository->createNewStub($characterData['corporationID'], $characterData['corporationName'], $alliance);
		$character = $this->charactersRepository->createNew($characterData['characterID'], $characterData['characterName'], $corp);

		if (!$apiKey->characters->has($character)) {
			$apiKey->characters->add($character);
			$this->keysRepository->persistAndFlush($apiKey);
		}

		return $character;
	}

	/**
	 * Fetches characters info and
	 */
	public function fillCharacters() {

		$characters = $this->charactersRepository->findUnfinished();
		do {

			$ids = [];
			foreach ($characters as $character) {
				$ids[] = $character->id;
			}

			$charactersData = $this->xmlApi->getCharacters($ids);

			foreach ($charactersData as $data) {
				$alliance = $this->alliancesRepository->createNewStub($data['allianceID'], $data['allianceName']);
				$corp = $this->corporationsRepository->createNewStub($data['corporationID'], $data['corporationName'], $alliance);
				$this->charactersRepository->createNew($data['characterID'], $data['characterName'], $corp);
			}
			$characters = $this->charactersRepository->findUnfinished();

		} while (count($characters));

	}
}
