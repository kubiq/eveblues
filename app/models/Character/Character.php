<?php

namespace EveBlues\Model\Character;

use EveBlues\Model\ApiKey\ApiKey;
use EveBlues\Model\Contact\AllianceContact;
use EveBlues\Model\Contact\CharacterContact;
use EveBlues\Model\Contact\ContactLabel;
use EveBlues\Model\Contact\CorporationContact;
use EveBlues\Model\Corporation\Corporation;
use EveBlues\Model\User\User;
use Nette\Utils\DateTime;
use Nextras\Orm\Entity\Entity;
use Nextras\Orm\Relationships\ManyHasMany;
use Nextras\Orm\Relationships\OneHasMany;

/**
 * Represents one Character
 *
 * @author Jakub Pistek <mail@jakubpistek.cz>
 *
 * @property int $id {primary}
 * @property string $name
 * @property DateTime|null $added {default now}
 * @property User|null $owner {m:1 User::$characters}
 * @property Corporation|null $corporation {m:1 Corporation::$characters}
 * @property string|null $token
 * @property DateTime|null $tokenExpires
 * @property boolean $isVisible
 * @property boolean $hasToken
 * @property boolean $isDownloaded
 *
 * @property OneHasMany|CharacterContact[] $contacts {1:m CharacterContact::$owner}
 * @property CorporationContact[] $corporationContacts {virtual}
 * @property AllianceContact[] $allianceContacts {virtual}
 * @property OneHasMany|ContactLabel[] $labels {1:m ContactLabel::$owner}
 * @property ManyHasMany|ApiKey[] $apiKeys {m:n ApiKey::$characters}
 *
 * @property-read string $nameWithContactsCount {virtual}
 */
class Character extends Entity {

	public function getterNameWithContactsCount() {

//		return "{$this->name} ({$this->contacts->count()}) ({$this->corporationContacts->count()}) ({$this->allianceContacts->count()})";
		return "{$this->name} ({$this->contacts->count()})";
	}

	public function getterCorporationContacts() {

		return $this->corporation->contacts;
	}

	public function getterAllianceContacts() {

		return $this->corporation->alliance ? $this->corporation->alliance->contacts : [];
	}
}
