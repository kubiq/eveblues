<?php

namespace EveBlues\Model\Alliance;

use EveBlues\Model\Corporation\CorporationsRepository;
use EveBlues\XmlApi\XmlApi;
use Kdyby\Monolog\Logger;
use Nextras\Dbal\Connection;
use Nextras\Dbal\Utils\DateTime;

/**
 * @author Jakub Pistek <mail@jakubpistek.cz>
 */
class AlliancesFacade {

	/** @var Logger */
	private $logger;
	/** @var AlliancesRepository */
	private $alliancesRepository;
	/** @var XmlApi */
	private $xmlApi;
	/** @var CorporationsRepository */
	private $corporationsRepository;
	/** @var Connection */
	private $connection;

	public function __construct(
		AlliancesRepository $alliancesRepository,
		CorporationsRepository $corporationsRepository,
		Connection $connection,
		XmlApi $xmlApi,
		Logger $logger
	) {
		$this->alliancesRepository = $alliancesRepository;
		$this->corporationsRepository = $corporationsRepository;
		$this->logger = $logger->channel('alliance');
		$this->xmlApi = $xmlApi;
		$this->connection = $connection;
	}

	public function refreshAlliances() {

		$alliancesData = $this->xmlApi->getAlliances();
		$count = count($alliancesData);
		$this->logger->addInfo("Refreshing {$count} alliances");

		foreach ($alliancesData as $row) {
			$this->createAlliance($row);
		}
	}

	private function createAlliance($data) {

		$values = [
			'id' => $data['allianceID'],
			'name' => $data['name'],
			'member_count' => $data['memberCount'],
			'start_date' => new DateTime($data['startDate']),
			'short_name' => $data['shortName'],
			'is_finished' => 0,
		];

		$this->connection->query(
			'INSERT INTO [alliances] %values ON DUPLICATE KEY UPDATE %set;', $values, $values
		);

		$this->createCorporations($data['allianceID'], $data['memberCorporations']);
	}

	private function createCorporations(int $allianceId, $corps) {
		$count = count($corps);
		$this->logger->addDebug("Refreshing {$count} corporations for alliance {$allianceId}");
		$this->connection->beginTransaction();
		$this->connection->query(
			'UPDATE [corporations] SET %set WHERE [alliance_id] = %i', ['alliance_id' => null], $allianceId
		);

		foreach ($corps as $row) {
			$corpsIds[] = $row['corporationID'];
			$values = [
				'id' => $row['corporationID'],
				'start_date' => new DateTime($row['startDate']),
				'alliance_id' => $allianceId,
			];
			$this->connection->query(
				'INSERT INTO [corporations] %values ON DUPLICATE KEY UPDATE %set;', $values, $values
			);
		}
		$this->connection->commitTransaction();
	}
}
