<?php

namespace EveBlues\Model\Alliance;

use Nextras\Orm\Mapper\Mapper;

/**
 * @author Jakub Pistek <mail@jakubpistek.cz>
 */
class AlliancesMapper extends Mapper {

	/** @var string */
	protected $tableName = 'alliances';

}
