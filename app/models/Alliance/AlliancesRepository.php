<?php

namespace EveBlues\Model\Alliance;

use Nextras\Orm\Repository\Repository;

/**
 * @author Jakub Pistek <mail@jakubpistek.cz>
 */
class AlliancesRepository extends Repository {

	/**
	 * @return string[]
	 */
	public static function getEntityClassNames() {
		return [Alliance::class];
	}

	public function createMany($alliancesData) {

		$alliances = [];
		foreach ($alliancesData as $allianceData) {
			$a = $this->create($allianceData);
			$this->persist($a);
			$alliances[$a->id] = $a;
		}

		$this->flush();

		return $alliances;
	}

	public function update(Alliance $alliance, array $corps, $data) :Alliance {

		$alliance->startDate = $data['startDate'];
		$alliance->deleted = $data['deleted'];
		$alliance->url = $data['url'];
		$alliance->isFinished = true;

		$alliance->corporationsCount = count($corps);
		$alliance->corporations->set($corps);

		$this->persistAndFlush($alliance);

		return $alliance;
	}

	/**
	 * @param int $id
	 * @param string $name
	 * @return Alliance|null
	 */
	public function createNewStub(int $id, string $name) {

		if (!$id) {
			return null;
		}

		$alliance = $this->getById($id);
		if (!$alliance) {
			$alliance = new Alliance();
			$alliance->id = $id;
			$alliance->name = $name;
			$alliance->isFinished = false;
			$this->persistAndFlush($alliance);
		}

		return $alliance;
	}

	private function create($allianceData) :Alliance {
		$alliance = $this->getById($allianceData['allianceID']);
		if (!$alliance) {
			$alliance = new Alliance();
			$alliance->id = $allianceData['allianceID'];
			$alliance->name = $allianceData['name'];
		}
		$alliance->shortName = $allianceData['shortName'];
		$alliance->startDate = $allianceData['startDate'];
		$alliance->corporationsCount = $allianceData['memberCount'];
		$alliance->isFinished = true;

		return $alliance;
	}

}
