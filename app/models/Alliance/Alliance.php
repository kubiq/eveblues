<?php

namespace EveBlues\Model\Alliance;

use EveBlues\Model\Contact\AllianceContact;
use EveBlues\Model\Corporation\Corporation;
use Nextras\Dbal\Utils\DateTime;
use Nextras\Orm\Entity\Entity;
use Nextras\Orm\Relationships\OneHasMany;

/**
 * Represents one Alliance
 *
 * @author Jakub Pistek <mail@jakubpistek.cz>
 *
 * @property int $id {primary}
 * @property string $name
 * @property string|null $shortName
 * @property DateTime|null $startDate
 * @property boolean|null $deleted
 * @property string|null $url
 * @property boolean $isFinished
 *
 * @property OneHasMany|Corporation[] $corporations {1:m Corporation::$alliance}
 * @property OneHasMany|AllianceContact[] $contacts {1:m AllianceContact::$owner}
 */
class Alliance extends Entity {

}
