<?php

namespace EveBlues\Model\AccessToken;

use Nextras\Orm\Mapper\Mapper;

/**
 * @author Jakub Pistek <mail@jakubpistek.cz>
 */
class AccessTokensMapper extends Mapper {

	/** @var string */
	protected $tableName = 'access_tokens';


}
