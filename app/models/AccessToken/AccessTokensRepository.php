<?php

namespace EveBlues\Model\AccessToken;

use Nextras\Orm\Repository\Repository;

/**
 * @author Jakub Pistek <mail@jakubpistek.cz>
 */
class AccessTokensRepository extends Repository {

	/**
	 * @return string[]
	 */
	public static function getEntityClassNames() {
		return [AccessToken::class];
	}

}
