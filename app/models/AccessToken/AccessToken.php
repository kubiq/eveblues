<?php

namespace EveBlues\Model\AccessToken;

use EveBlues\Model\Character\Character;
use Nette\Utils\DateTime;
use Nextras\Orm\Entity\Entity;

/**
 * Represents one Crest token
 *
 * @author Jakub Pistek <mail@jakubpistek.cz>
 *
 * @property int $id {primary}
 * @property Character $character {1:1 Character::$accessToken, isMain=true}
 * @property DateTime $added {default now}
 * @property string $token
 * @property DateTime|null $expiresOn
 */
class AccessToken extends Entity {

}
