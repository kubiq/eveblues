<?php

namespace EveBlues\Model\AccessToken;

use EveBlues\Auth\EveSSOFactory;
use EveBlues\Model\Character\Character;
use EveBlues\Model\Character\CharactersRepository;
use Evelabs\OAuth2\Client\Provider\EveOnline;
use Kdyby\Monolog\Logger;
use League\OAuth2\Client\Token\AccessToken as EveToken;

/**
 * @author Jakub Pistek <mail@jakubpistek.cz>
 */
class AccessTokensFacade {

	/** @var Logger */
	private $logger;
	/** @var CharactersRepository */
	private $charactersRepository;
	/** @var EveOnline */
	private $eveSSOProvider;
	/** @var AccessTokensRepository */
	private $repository;

	public function __construct(
		CharactersRepository $charactersRepository,
		EveSSOFactory $eveSSOFactory,
		Logger $logger
	) {
		$this->charactersRepository = $charactersRepository;
		$this->logger = $logger->channel('token');
		$this->eveSSOProvider = $eveSSOFactory->createNew();;
	}

	public function getAuthCode() {

		// If we don't have an authorization code then get one
		$authUrl = $this->eveSSOProvider->getAuthorizationUrl([
			'scope' => ['characterContactsRead', 'characterContactsWrite']
		]);
		$_SESSION['oauth2state'] = $this->eveSSOProvider->getState();
		header('Location: ' . $authUrl);
		exit;
	}

	public function getToken(string $code) :EveToken {

		$token = $this->eveSSOProvider->getAccessToken('authorization_code', [
			'code' => $code
		]);

		return $token;
	}

	public function saveToken(Character $character, EveToken $token) {

		$accessToken = $this->repository->getBy([
			'character' => $character
		]);
		if (!$accessToken) {
			$accessToken = new AccessToken();
			$accessToken->character = $character;
		}
		$accessToken->expiresOn = $token->getExpires();
		$accessToken->token = $token->getToken();

		$this->repository->persistAndFlush($accessToken);

		return $accessToken;
	}





}
