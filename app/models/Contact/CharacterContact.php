<?php

namespace EveBlues\Model\Contact;

use EveBlues\Model\Character\Character;

/**
 * Represents Alliance contact
 *
 * @author Jakub Pistek <mail@jakubpistek.cz>
 *
 * @property int $id {primary}
 * @property Character $owner {m:1 Character::$contacts}
 * @property int $subjectId
 * @property string $subjectName
 * @property int $standing
 * @property int $type {enum self::TYPE_*}
 * @property ContactLabel|null $label {m:1 ContactLabel::$contacts}
 * @property boolean|null $inWatchlist
 */
class CharacterContact extends Contact {

}
