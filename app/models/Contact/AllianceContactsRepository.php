<?php

namespace EveBlues\Model\Contact;

use EveBlues\Model\Alliance\Alliance;
use Nextras\Orm\Repository\Repository;

/**
 * @author Jakub Pistek <mail@jakubpistek.cz>
 */
class AllianceContactsRepository extends Repository {

	/**
	 * @return string[]
	 */
	public static function getEntityClassNames() {
		return [AllianceContact::class];
	}

	/**
	 * @param Alliance $owner
	 * @param array $data
	 * @return AllianceContact[]
	 */
	public function createNew(Alliance $owner, array $data) {
		$contacts = [];
		foreach ($data as $contactData) {

			$contact = $this->getBy([
				'owner' => $owner,
				'subjectId' => $contactData['contactID']
			]);

			if (!$contact) {

				$contact = new AllianceContact();
				$contact->owner = $owner;
				$contact->subjectId = $contactData['contactID'];
				$contact->subjectName = $contactData['contactName'];
				$contact->type = Contact::getContactType($contactData['contactTypeID']);

			}

			$contact->standing = $contactData['standing'];
			$this->persist($contact);
			$contacts[$contact->id] = $contact;
		}

		$this->flush();

		return $contacts;
	}
}
