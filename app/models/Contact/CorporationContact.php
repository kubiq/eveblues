<?php

namespace EveBlues\Model\Contact;

use EveBlues\Model\Corporation\Corporation;

/**
 * Represents Alliance contact
 *
 * @author Jakub Pistek <mail@jakubpistek.cz>
 *
 * @property int $id {primary}
 * @property Corporation $owner {m:1 Corporation::$contacts}
 * @property int $subjectId
 * @property string $subjectName
 * @property int $standing
 * @property int $type {enum self::TYPE_*}
 */
class CorporationContact extends Contact {

}
