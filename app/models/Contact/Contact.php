<?php

namespace EveBlues\Model\Contact;

use Nette\InvalidStateException;
use Nextras\Orm\Entity\Entity;

/**
 * @author Jakub Pistek <mail@jakubpistek.cz>
 *
 * @property int $subjectId
 * @property string $subjectName
 * @property int $standing
 * @property int $type {enum self::TYPE_*}
 */
abstract class Contact extends Entity {

	const TYPE_CHARACTER = 1;
	const TYPE_CORPORATION = 2;
	const TYPE_ALLIANCE = 3;

	/**
	 * @param int $type
	 *
	 * @return int
	 */
	public static function getContactType(int $type) :int {

		switch ($type) {
			case 2:
				return self::TYPE_CORPORATION;
			case 16159:
				return self::TYPE_ALLIANCE;
			case 1373:
			case 1374:
			case 1375:
			case 1376:
			case 1377:
			case 1378:
			case 1379:
			case 1380:
			case 1381:
			case 1382:
			case 1383:
			case 1384:
			case 1385:
			case 1386:
				return self::TYPE_CHARACTER;
			default:
				throw new InvalidStateException('Unknown contact type');
		}

	}
}
