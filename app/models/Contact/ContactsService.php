<?php

namespace EveBlues\Model\Contact;

use EveBlues\Model\Character\Character;
use EveBlues\Model\Character\CharactersRepository;
use Nextras\Dbal\Connection;

/**
 * @author Jakub Pistek <mail@jakubpistek.cz>
 *
 */
class ContactsService {

	/** @var Connection */
	private $connection;

	public function __construct(
		Connection $connection
	) {
		$this->connection = $connection;
	}

	public function filterContacts($characterContacts, $corporationContacts, $allianceContacts) {

		$allianceContacts = $this->indexContacts($allianceContacts);
		$corporationContacts = $this->indexContacts($corporationContacts);
		$characterContacts = $this->indexContacts($characterContacts);

		\Tracy\Debugger::barDump(count($characterContacts), 'character pred fultorvanim');

		$filteredCharacter = array_filter($characterContacts, function ($c) use ($corporationContacts, $allianceContacts) {
			if ($this->hasParentContact($corporationContacts, $c)) {
				return false;
			}

			if ($this->hasParentContact($allianceContacts, $c)) {
				return false;
			}
			return true;
		});

		\Tracy\Debugger::barDump(count($filteredCharacter), 'character po filtrovani');
		\Tracy\Debugger::barDump(count($corporationContacts), 'corp pred filtrovanim');

		$filteredCorporation = array_filter($corporationContacts, function ($c) use ($allianceContacts) {
			if ($this->hasParentContact($allianceContacts, $c)) {
				return false;
			}

			return true;
		});

		\Tracy\Debugger::barDump(count($filteredCorporation), 'cor po filtrovani');

		\Tracy\Debugger::barDump(count($filteredCharacter), 'characters pred filtrovanim');

		$filteredCharacter = array_filter($filteredCharacter, function ($c) use ($filteredCharacter) {
			if ($c->type === Contact::TYPE_CHARACTER) {

				$corpId = $this->getCorpByCharId($c->subjectId);

				if (isset($filteredCharacter[$corpId])) {
					return false;
				}
			}

			return true;
		});

		\Tracy\Debugger::barDump(count($filteredCharacter), 'chars po ');

		\Tracy\Debugger::barDump(count($filteredCharacter), 'characters pred alli filtrovanim');

		$filteredCharacter = array_filter($filteredCharacter, function ($c) use ($filteredCharacter) {
			if ($c->type === Contact::TYPE_CHARACTER) {

				$allianceId = $this->getAllianceByCharId($c->subjectId);

				if (isset($filteredCharacter[$allianceId])) {
					return false;
				}
			}

			return true;
		});

		\Tracy\Debugger::barDump(count($filteredCharacter), 'chars po alli');

		return [];
	}

	public function indexContacts($contacts) {
		$indexed = [];
		foreach ($contacts as $c) {
			$indexed[$c->subjectId] = $c;
		}

		return $indexed;
	}

	/**
	 * Returns true if parent group has contact set too, so child does not have to have it
	 *
	 * @param array $parent
	 * @param Contact $contact
	 * @return bool
	 */
	public function hasParentContact(array $parent, Contact $contact) {
		return isset($parent[$contact->subjectId]);
	}

	private function getCorpByCharId(int $id) {

		return $this->connection->query('SELECT corporation_id FROM characters WHERE id = %i', $id)->fetchField();
	}

	private function getAllianceByCharId(int $id) {

		return $this->connection->query(
			'SELECT co.alliance_id 
			FROM characters ch 
			JOIN corporations co 
			ON (ch.corporation_id = co.id) 
			WHERE ch.id = %i',
			$id)->fetchField();
	}
}
