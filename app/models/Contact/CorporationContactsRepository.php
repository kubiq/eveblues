<?php

namespace EveBlues\Model\Contact;

use EveBlues\Model\Corporation\Corporation;
use Nextras\Orm\Repository\Repository;

/**
 * @author Jakub Pistek <mail@jakubpistek.cz>
 */
class CorporationContactsRepository extends Repository {

	/**
	 * @return string[]
	 */
	public static function getEntityClassNames() {
		return [CorporationContact::class];
	}

	/**
	 * @param Corporation $owner
	 * @param array $data
	 * @return CorporationContact[]
	 */
	public function createNew(Corporation $owner, array $data) {
		$contacts = [];
		foreach ($data as $contactData) {

			$contact = $this->getBy([
				'owner' => $owner,
				'subjectId' => $contactData['contactID']
			]);

			if (!$contact) {

				$contact = new CorporationContact();
				$contact->owner = $owner;
				$contact->subjectId = $contactData['contactID'];
				$contact->subjectName = $contactData['contactName'];
				$contact->type = Contact::getContactType($contactData['contactTypeID']);

			}
			$contact->standing = $contactData['standing'];
			$this->persist($contact);
			$contacts[$contact->id] = $contact;
		}

		$this->flush();

		return $contacts;
	}
}
