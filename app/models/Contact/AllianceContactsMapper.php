<?php

namespace EveBlues\Model\Contact;

use Nextras\Orm\Mapper\Mapper;

/**
 * @author Jakub Pistek <mail@jakubpistek.cz>
 */
class AllianceContactsMapper extends Mapper {

	/** @var string */
	protected $tableName = 'alliance_contacts';

}
