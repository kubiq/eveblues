<?php

namespace EveBlues\Model\Contact;

use Nextras\Orm\Mapper\Mapper;

/**
 * @author Jakub Pistek <mail@jakubpistek.cz>
 */
class CorporationContactsMapper extends Mapper {

	/** @var string */
	protected $tableName = 'corporation_contacts';

}
