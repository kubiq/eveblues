<?php

namespace EveBlues\Model\Contact;

use EveBlues\Model\Alliance\Alliance;

/**
 * Represents Alliance contact
 *
 * @author Jakub Pistek <mail@jakubpistek.cz>
 *
 * @property int $id {primary}
 * @property Alliance $owner {m:1 Alliance::$contacts}
 * @property int $subjectId
 * @property string $subjectName
 * @property int $standing
 * @property int $type {enum self::TYPE_*}
 */
class AllianceContact extends Contact {

}
