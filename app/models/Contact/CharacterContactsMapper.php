<?php

namespace EveBlues\Model\Contact;

use Nextras\Orm\Mapper\Mapper;

/**
 * @author Jakub Pistek <mail@jakubpistek.cz>
 */
class CharacterContactsMapper extends Mapper {

	/** @var string */
	protected $tableName = 'character_contacts';

}
