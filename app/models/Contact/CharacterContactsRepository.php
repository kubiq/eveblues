<?php

namespace EveBlues\Model\Contact;

use EveBlues\Model\Character\Character;
use Nextras\Orm\Repository\Repository;

/**
 * @author Jakub Pistek <mail@jakubpistek.cz>
 */
class CharacterContactsRepository extends Repository {

	/**
	 * @return string[]
	 */
	public static function getEntityClassNames() {
		return [CharacterContact::class];
	}

	/**
	 * @param Character $owner
	 * @param array $labels
	 * @param array $data
	 * @return CharacterContact[]
	 */
	public function createNew(Character $owner, array $labels = null, array $data) {
		$contacts = [];
		foreach ($data as $contactData) {

			if ($contactData['standing'] == 0) {
				continue;
			}

			$contact = $this->getBy([
				'owner' => $owner,
				'subjectId' => $contactData['contactID']
			]);

			if (!$contact) {
				$contact = new CharacterContact();
				$contact->owner = $owner;
				$contact->subjectId = $contactData['contactID'];
				$contact->subjectName = $contactData['contactName'];
				$contact->standing = $contactData['standing'];
				$contact->type = Contact::getContactType($contactData['contactTypeID']);
			}

			if ($labels && isset($labels[(int)$contactData['labelMask']])) {
				$label = $labels[(int)$contactData['labelMask']];
			} else {
				$label = null;
			}

			$inWatchlist = isset($contactData['inWatchlist']) && $contactData['inWatchlist'] == 'True';
			$contact->label = $label;
			$contact->inWatchlist = $inWatchlist;
			$this->persist($contact);
			$contacts[$contact->id] = $contact;
		}

		$this->flush();

		return $contacts;
	}
}
