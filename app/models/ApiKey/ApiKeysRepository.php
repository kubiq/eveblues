<?php

namespace EveBlues\Model\ApiKey;

use EveBlues\Model\Character\Character;
use EveBlues\Model\User\User;
use Nextras\Orm\Repository\Repository;

/**
 * @author Jakub Pistek <mail@jakubpistek.cz>
 */
class ApiKeysRepository extends Repository {

	/**
	 * @return string[]
	 */
	public static function getEntityClassNames() {
		return [ApiKey::class];
	}

	/**
	 * @param User $owner
	 * @return ApiKey[]
	 */
	public function findByOwner(User $owner) {

		return $this->findBy([
			'owner' => $owner,
			'removed' => false
		]);
	}

	public function createNew(User $owner, string $keyId, string $vCode, $label, array $keyData) :ApiKey {

		$key = $this->getBy([
			'owner' => $owner,
			'keyId' => $keyId,
			'vCode' => $vCode
		]);
		if (!$key) {
			$key = new ApiKey();
			$key->owner = $owner;
			$key->keyId = $keyId;
			$key->vCode = $vCode;
		}

		$key->removed = false;
		$key->label = $label ?: null;
		$key->expires = $keyData['expires'] ?: null;
		$key->accessMask = $keyData['accessMask'];
		$key->type = ApiKey::getType($keyData['type']);

		$this->persistAndFlush($key);

		return $key;
	}
}
