<?php

namespace EveBlues\Model\ApiKey;

use EveBlues\Model\Character\Character;
use EveBlues\Model\User\User;
use Nette\Utils\DateTime;
use Nextras\Orm\Entity\Entity;
use Nextras\Orm\Relationships\ManyHasMany;

/**
 * Represents one Api key
 *
 * @author Jakub Pistek <mail@jakubpistek.cz>
 *
 * @property int $id {primary}
 * @property User $owner {m:1 User::$apiKeys}
 * @property string|null $label 
 * @property string $keyId
 * @property string $vCode 
 * @property DateTime $added {default now}
 * @property DateTime|null $expires
 * @property int $type {enum self::TYPE_*}
 * @property int $accessMask
 * @property boolean $removed
 * 
 * @property ManyHasMany|Character[] $characters {m:n Character::$apiKeys, isMain=true}
 * @property-read Character[] $charactersPairs {virtual}
 */
class ApiKey extends Entity {

	const TYPE_CHARACTER = 1;

	const TYPE_ACCOUNT = 2;

	const TYPE_CORPORATION = 3;

	const TYPE_ALLIANCE = 4;

	public static function getType(string $type) {
		switch ($type) {
			case 'Character':
				return self::TYPE_CHARACTER;
			case 'Account':
				return self::TYPE_ACCOUNT;
			case 'Corporation':
				return self::TYPE_CORPORATION;
			case 'Alliance':
				return self::TYPE_ALLIANCE;
		}
		return null;
	}

	public function getterCharactersPairs() {
		$pairs = [];
		foreach ($this->characters as $c) {
			$pairs[$c->id] = $c->name;
		}
		return $pairs;
	}
}
