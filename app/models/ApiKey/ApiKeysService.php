<?php

namespace EveBlues\Model\ApiKey;


/**
 * @author Jakub Pistek <mail@jakubpistek.cz>
 */
class ApiKeysService {

	public function isKeyValid(ApiKey $apiKey) {

		return $apiKey->accessMask & 8388624; // 0x800010
	}
}
