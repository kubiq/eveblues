<?php

namespace EveBlues\Model\ApiKey;

use EveBlues\Api\PhealFactory;
use EveBlues\Model\Character\CharactersFacade;
use EveBlues\Model\Character\CharactersRepository;
use EveBlues\Model\User\User;
use Nette\Application\BadRequestException;
use Nette\Application\ForbiddenRequestException;

/**
 * @author Jakub Pistek <mail@jakubpistek.cz>
 */
class ApiKeysFacade {

	/** @var ApiKeysRepository */
	private $repository;
	/** @var PhealFactory */
	private $pheal;
	/** @var CharactersRepository */
	private $charactersRepository;
	/**
	 * @var CharactersFacade
	 */
	private $charactersFacade;

	/**
	 * @param ApiKeysRepository $apiKeysRepository
	 * @param CharactersRepository $charactersRepository
	 * @param PhealFactory $pheal
	 */
	public function __construct(
		ApiKeysRepository $apiKeysRepository,
		CharactersFacade $charactersFacade,
		CharactersRepository $charactersRepository,
		PhealFactory $pheal
	) {
		$this->repository = $apiKeysRepository;
		$this->pheal = $pheal;
		$this->charactersRepository = $charactersRepository;
		$this->charactersFacade = $charactersFacade;
	}

	public function saveNewKey(User $user, string $keyId, string $vCode, $label) :ApiKey {

		$pheal = $this->pheal->createNew($keyId, $vCode, PhealFactory::SCOPE_ACCOUNT);

		$result = $pheal->detectAccess();
		$keyData = $result->toArray()['result']['key'];

		$key = $this->repository->createNew($user, $keyId, $vCode, $label, $keyData);

		$this->saveCharacters($key, $keyData['characters']);

		return $key;
	}

	private function saveCharacters(ApiKey $key, $characters) {

		foreach ($characters as $character) {
			$this->charactersFacade->createNewFromApiKey($key, $character);
		}
	}

	public function removeKey(User $user, int $id) :ApiKey {

		$key = $this->repository->getById($id);
		if (!$key) {
			throw new BadRequestException();
		}
		if ($key->owner !== $user) {
			throw new ForbiddenRequestException();
		}

		$key->removed = true;
		$this->repository->persistAndFlush($key);

		return $key;
	}
}
