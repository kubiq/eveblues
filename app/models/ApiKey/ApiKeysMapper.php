<?php

namespace EveBlues\Model\ApiKey;

use Nextras\Orm\Mapper\Mapper;

/**
 * @author Jakub Pistek <mail@jakubpistek.cz>
 */
class ApiKeysMapper extends Mapper {

	/** @var string */
	protected $tableName = 'api_keys';

	/**
	 * @return \Nextras\Orm\Mapper\Dbal\StorageReflection\UnderscoredStorageReflection
	 */
	public function createStorageReflection() {

		$reflection = parent::createStorageReflection();
		$reflection->addMapping('keyId', 'keyid');
		$reflection->addMapping('vCode', 'vcode');

		return $reflection;
	}

}
