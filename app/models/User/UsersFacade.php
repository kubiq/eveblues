<?php

namespace EveBlues\Model\User;

use EveBlues\Model\Character\CharactersRepository;
use Kdyby\Monolog\Logger;

/**
 * @author Jakub Pistek <mail@jakubpistek.cz>
 */
class UsersFacade {

	/** @var Logger */
	private $logger;
	/** @var UsersRepository */
	private $usersRepository;
	/** @var CharactersRepository */
	private $charactersRepository;

	public function __construct(
		CharactersRepository $charactersRepository,
		UsersRepository $usersRepository,
		Logger $logger
	) {
		$this->usersRepository = $usersRepository;
		$this->charactersRepository = $charactersRepository;
		$this->logger = $logger->channel('user');
	}
}
