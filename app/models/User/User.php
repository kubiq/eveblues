<?php

namespace EveBlues\Model\User;

use EveBlues\Model\ApiKey\ApiKey;
use EveBlues\Model\Character\Character;
use Nette\Utils\DateTime;
use Nextras\Orm\Entity\Entity;
use Nextras\Orm\Relationships\OneHasMany;

/**
 * Represents one website user
 *
 * @author Jakub Pistek <mail@jakubpistek.cz>
 *
 * @property int $id {primary}
 * @property string $name
 * @property DateTime $registered {default now}
 *
 * @property OneHasMany|Character[] $characters {1:m Character::$owner}
 * @property OneHasMany|ApiKey[] $apiKeys {1:m ApiKey::$owner}
 */
class User extends Entity {

}
