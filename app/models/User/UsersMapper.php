<?php

namespace EveBlues\Model\User;

use Nextras\Orm\Mapper\Mapper;

/**
 * @author Jakub Pistek <mail@jakubpistek.cz>
 */
class UsersMapper extends Mapper {

	/** @var string */
	protected $tableName = 'users';


}
