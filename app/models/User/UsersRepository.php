<?php

namespace EveBlues\Model\User;

use EveBlues\Model\Character\Character;
use Nextras\Orm\Repository\Repository;

/**
 * @author Jakub Pistek <mail@jakubpistek.cz>
 */
class UsersRepository extends Repository {

	/**
	 * @return string[]
	 */
	public static function getEntityClassNames() {
		return [User::class];
	}

	public function createNew(string $name) :User {

		$user = $this->getBy(['name' => $name]);
		if (!$user) {
			$user = new User();
			$user->name = $name;
			$this->persistAndFlush($user);
		}

		return $user;
	}

	/**
	 * @param int $characterId
	 * @return User
	 */
	public function getByCharacterId(int $characterId) {

		return $this->getBy([
			'this->characters->id' => $characterId
		]);
	}

	public function addCharacter(User $user, Character $character) :User {

		$user->characters->add($character);
		$this->persistAndFlush($user);

		return $user;
	}
}
