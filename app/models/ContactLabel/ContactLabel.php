<?php

namespace EveBlues\Model\Contact;

use EveBlues\Model\Character\Character;
use Nextras\Orm\Entity\Entity;
use Nextras\Orm\Relationships\OneHasMany;

/**
 * Represents contact label
 *
 * @author Jakub Pistek <mail@jakubpistek.cz>
 *
 * @property int $id {primary}
 * @property int $labelId
 * @property string $name
 * @property Character $owner {m:1 Character::$labels}
 * @property OneHasMany|CharacterContact[] $contacts {1:m CharacterContact::$label}
 */
class ContactLabel extends Entity {

}
