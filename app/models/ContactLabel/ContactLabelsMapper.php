<?php

namespace EveBlues\Model\Contact;

use Nextras\Orm\Mapper\Mapper;

/**
 * @author Jakub Pistek <mail@jakubpistek.cz>
 */
class ContactLabelsMapper extends Mapper {

	/** @var string */
	protected $tableName = 'contact_labels';

	/**
	 * @return \Nextras\Orm\Mapper\Dbal\StorageReflection\UnderscoredStorageReflection
	 */
	public function createStorageReflection() {

		$reflection = parent::createStorageReflection();

		return $reflection;
	}


}
