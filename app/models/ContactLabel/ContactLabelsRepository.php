<?php

namespace EveBlues\Model\Contact;

use EveBlues\Model\Character\Character;
use Kdyby\Monolog\Logger;
use Nextras\Orm\Mapper\IMapper;
use Nextras\Orm\Repository\IDependencyProvider;
use Nextras\Orm\Repository\Repository;

/**
 * @author Jakub Pistek <mail@jakubpistek.cz>
 */
class ContactLabelsRepository extends Repository {

	/** @var Logger */
	private $logger;

	/**
	 * UsersRepository constructor.
	 *
	 * @param IMapper $mapper
	 * @param IDependencyProvider $dependencyProvider
	 * @param Logger $logger
	 */
	public function __construct(
		IMapper $mapper,
		IDependencyProvider $dependencyProvider,
		Logger $logger
	) {
		parent::__construct($mapper, $dependencyProvider);

	}

	/**
	 * @return string[]
	 */
	public static function getEntityClassNames() {
		return [ContactLabel::class];
	}

	public function createNew(Character $character, array $data) {
		$labels = [];

		foreach ($data as $labelData) {
			$label = $this->getBy([
				'owner' => $character,
				'labelId' => $labelData['labelID']
			]);

			if (!$label) {
				$label = new ContactLabel();
				$label->owner = $character;
				$label->labelId = $labelData['labelID'];
				$label->name = $labelData['name'];
				$this->persist($label);
			}
			$labels[$label->labelId] = $label;
		}
		$this->flush();

		return $labels;
	}
}
