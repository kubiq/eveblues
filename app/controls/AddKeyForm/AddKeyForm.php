<?php

namespace EveBlues\Controls;

use EveBlues\Model\ApiKey\ApiKeysFacade;
use Nette\Application\UI\Form;
use Nette\ComponentModel\IComponent;
use Nette\Forms\Controls\SubmitButton;
use Pheal\Exceptions\PhealException;

/**
 * @author Jakub Pistek <mail@jakubpistek.cz>
 */
class AddKeyForm extends Control {

	/** @var ApiKeysFacade */
	public $apiKeysFacade;

	public $characters;

	public function __construct(
		ApiKeysFacade $apiKeysFacade
	) {
		$this->apiKeysFacade = $apiKeysFacade;
		$this->characters = [];
	}

	/**
	 * @return Form $form
	 */
	protected function createComponentForm() {
		$form = new Form();

		$form->addText('keyId', 'Key ID')
			->setType('text')
			->setRequired(true)
			->controlPrototype->setAttribute('placeholder', '123456789');

		$form->addText('vCode', 'Verification Code')
			->setType('text')
			->setRequired(true)
			->controlPrototype->setAttribute('placeholder', 'qwertuiopasdfhjklzxcvbnm');

		$form->addText('label', 'Label')
			->setType('text')
			->controlPrototype->setAttribute('placeholder', 'Main toon');

		$form->addSubmit('add', 'Select character')
			->onClick[] = [$this->presenter, 'handleAdd'];

		return $form;
	}

	public function addKey(Form $form, $values) {

		try {
			$key = $this->apiKeysFacade->saveNewKey($this->presenter->loggedInUser, $values['keyId'], $values['vCode'], $values['label']);

			$this->flashMessage('Key added!', 'success');
			$this->presenter->redirect('Key:selectCharacter', ['id' => $key->id]);

		} catch (PhealException $ex) {
			$this->flashMessage('The key is invalid or expired', 'warning');
		}
	}

	public function render() {
		$tpl = __DIR__ . '/control.latte';
		$this->template->setFile($tpl);
		$this->template->render();
	}


}
