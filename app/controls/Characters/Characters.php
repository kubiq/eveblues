<?php

namespace EveBlues\Controls;

/**
 * @author Jakub Pistek <mail@jakubpistek.cz>
 */
class Characters extends Control {

	public function render($characters) {
		$tpl = __DIR__ . '/control.latte';
		$this->template->setFile($tpl);
		$this->template->characters = $characters;
		$this->template->render();
	}

}
