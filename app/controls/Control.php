<?php

namespace EveBlues\Controls;

use Nette\Application\UI\Control as NControl;

/**
 * @author Jakub Pistek <mail@jakubpistek.cz>
 */
class Control extends NControl {

	public function flashMessage($message, $type = 'info') {
		$this->presenter->redrawControl('flashes');
		$this->presenter->flashMessage($message, $type);
	}

}
