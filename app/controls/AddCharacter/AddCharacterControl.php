<?php

namespace EveBlues\Controls;

use Nette\Application\UI\Control;
use Nette\Application\UI\Form;

/**
 * @author Jakub Pistek <mail@jakubpistek.cz>
 */
class AddCharacterControl extends Control {

	/**
	 * @return Form $form
	 */
	protected function createComponentForm() {
		$form = new Form();

		$form->addSubmit('add', 'Add character');

		$form->onSuccess[] = [$this, 'add'];
		
		return $form;
	}

	public function add(Form $form, $values) {
		
	}

	public function render() {
		$tpl = __DIR__ . '/control.latte';
		$this->template->setFile($tpl);
		$this->template->render();
	}

}
