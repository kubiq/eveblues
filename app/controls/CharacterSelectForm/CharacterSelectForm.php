<?php

namespace EveBlues\Controls;

use App\Presenters\CharactersPresenter;
use App\Presenters\KeyPresenter;
use EveBlues\Model\ApiKey\ApiKey;
use EveBlues\Model\ApiKey\ApiKeysFacade;
use EveBlues\Model\ApiKey\ApiKeysRepository;
use EveBlues\Model\Contact\ContactsFacade;
use Nette\Application\BadRequestException;
use Nette\Application\UI\Form;
use Nette\Http\Session;
use Nette\Http\SessionSection;

/**
 * @author Jakub Pistek <mail@jakubpistek.cz>
 * @property CharactersPresenter $presenter
 */
class CharacterSelectForm extends Control {

	/** @var ApiKeysFacade */
	public $apiKeysFacade;
	/** @var ContactsFacade */
	public $contactsFacade;
	/** @var ApiKeysRepository */
	private $repository;
	/** @var SessionSection */
	private $session;
	/** @var ApiKey */
	private $apiKey;

	public function __construct(
		ApiKeysFacade $apiKeysFacade,
		ApiKeysRepository $repository,
		ContactsFacade $contactsFacade,
		Session $session
	) {
		$this->apiKeysFacade = $apiKeysFacade;
		$this->repository = $repository;
		$this->contactsFacade = $contactsFacade;
		$this->session = $session->getSection('apiKeyId');

		$this->apiKey = $this->repository->getById($this->session->id);

		if (!$this->apiKey) {
			throw new BadRequestException();
		}
	}

	/**
	 * @return Form $form
	 */
	protected function createComponentForm() {
		$form = new Form();

		$form->addSelect('character', 'Character', $this->apiKey->charactersPairs)
			->setRequired(true);

		$form->addSubmit('select', 'Get contacts');
		$form->onSubmit[] = [$this, 'getContacts'];

		return $form;
	}

	public function getContacts(Form $form) {

		$values = $form->getValues();
		$character = $this->contactsFacade->getContacts($this->presenter->loggedInUser, $this->apiKey, $values['character']);

		$this->flashMessage("Saved {$character->contacts->count()} contacts for {$character->name}.");
		$this->presenter->redirect('Characters:');
	}

	public function render() {
		$tpl = __DIR__ . '/control.latte';
		$this->template->setFile($tpl);
		$this->template->render();
	}


}
