<?php

namespace EveBlues\Controls;

use EveBlues\Model\Character\Character;
use EveBlues\Model\Character\CharactersRepository;
use EveBlues\Model\Contact\ContactsFacade;
use Nette\Application\UI\Form;

/**
 * @author Jakub Pistek <mail@jakubpistek.cz>
 */
class CopyContactsForm extends Control {

	/** @var CharactersRepository */
	private $charactersRepository;
	/** @var Character[] */
	private $downloadedChars;
	/** @var ContactsFacade */
	private $contactsFacade;

	public function __construct(
		CharactersRepository $charactersRepository,
		ContactsFacade $contactsFacade
	) {
		$this->charactersRepository = $charactersRepository;
		$this->downloadedChars = [];
		$this->contactsFacade = $contactsFacade;
	}


	/**
	 * @return Form $form
	 */
	protected function createComponentForm() {
		$form = new Form();
		/** @var Character[] $characters */
		$downloadedChars = $this->charactersRepository->findDownloaded($this->presenter->loggedInUser);
		foreach ($downloadedChars as $c) {
			$this->downloadedChars[$c->id] = $c->nameWithContactsCount;
		}

		/** @var Character[] $characters */
		$characters = $this->charactersRepository->findWithToken($this->presenter->loggedInUser);
		$withToken = [];
		foreach ($characters as $character) {
			$withToken[$character->id] = $character->name;
		}

		$form->addSelect('from', 'Contacts from character', $this->downloadedChars);

		$form->addSelect('to', 'Character where to copy contacts', $withToken);

		$form->addSubmit('copy', 'Copy contacts');

		$form->onSuccess[] = [$this, 'copy'];

		return $form;
	}

	public function copy(Form $form) {
		$values = $form->getValues();

		$characterFrom = $this->charactersRepository->getById($values['from']);
		$characterTo = $this->charactersRepository->getById($values['to']);

//		$this->presenter->redirect('Contacts:progress');
//		flush();

		$this->contactsFacade->copyContacts($characterFrom, $characterTo);

	}

	public function render() {
		$tpl = __DIR__ . '/control.latte';
		$this->template->setFile($tpl);
		$this->template->downloadedChars = $this->downloadedChars;
		$this->template->render();
	}

}
