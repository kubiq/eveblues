<?php

namespace App\Presenters;

use EveBlues\Crest\Crest;
use EveBlues\Model\ApiKey\ApiKeysRepository;
use EveBlues\Model\Character\CharactersFacade;
use EveBlues\Model\Character\CharactersRepository;
use EveBlues\Model\Contact\ContactsFacade;

/**
 * Class StandingsPresenter
 * @package App\Presenters
 * @User
 */
class ContactsPresenter extends BasePresenter {

	/** @var ApiKeysRepository @inject */
	public $apiKeysRepository;
	/** @var ContactsFacade @inject */
	public $contactsFacade;
	/** @var CharactersRepository @inject */
	public $charactersRepository;
	/** @var CharactersFacade @inject */
	public $charactersFacade;
	/** @var Crest @inject */
	public $crest;

	public function startup() {
		parent::startup();
		if (!$this->user->isLoggedIn()) {
			$this->flashMessage('You have to be logged in.');
			$this->redirect('Homepage:');
		}
	}

	public function renderDefault() {



		$characters = $this->charactersRepository->findByOwner($this->loggedInUser);

		$character = $this->charactersRepository->getById(2023798312);


		$this->contactsFacade->copyContacts($character, $character);


		$this->template->characters = $characters;

	}

	public function actionCopy() {

		$this->redirect('Characters:');
	}

	public function actionProgress() {

		$this->redirect('Characters:');
	}

}
