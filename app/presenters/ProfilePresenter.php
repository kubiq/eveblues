<?php

namespace App\Presenters;

use EveBlues\Api\PhealFactory;
use EveBlues\Auth\EveSSOFactory;
use EveBlues\Controls\AddCharacterControl;
use EveBlues\Controls\CharactersControl;
use EveBlues\Model\Contact\ContactsFacade;
use Evelabs\OAuth2\Client\Provider\EveOnline;
use Nette;
use Nette\Application\UI\Form;

/**
 * Class ProfilePresenter
 * @package App\Presenters
 * @User
 */
class ProfilePresenter extends BasePresenter {

}
