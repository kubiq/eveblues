<?php

namespace App\Presenters;

use EveBlues\Api\PhealFactory;
use EveBlues\Model\ApiKey\ApiKey;
use EveBlues\Model\ApiKey\ApiKeysFacade;
use EveBlues\Model\ApiKey\ApiKeysRepository;
use Nette;
use Nette\Application\UI\Form;
use Nette\Forms\Controls\SubmitButton;

/**
 * Class StandingsPresenter
 * @package App\Presenters
 * @User
 */
class KeyPresenter extends BasePresenter {

	/** @var ApiKeysRepository @inject */
	public $apiKeysRepository;
	/** @var ApiKeysFacade @inject */
	public $apiKeysFacade;
	/** @var PhealFactory @inject */
	public $pheal;
	/** @var Nette\Http\Session */
	public $keySession;
	/** @var int */
	public $step;
	/** @var ApiKey */
	public $key;

	public function startup() {
		parent::startup();
		if (!$this->user->isLoggedIn()) {
			$this->flashMessage('You have to be logged in.');
			$this->redirect('Homepage:');
		}

		$this->keySession = $this->getSession('apiKeyId');
	}

	public function renderDefault() {
		$apiKeys = $this->apiKeysRepository->findByOwner($this->loggedInUser);
		$this->template->keys = $apiKeys;
	}

	public function renderAdd() {
		if (!$this->step) {
			$this->step = 1;
		}
		$this->template->step = $this->step;
	}

	public function actionRemove($id = null) {

		if ($id) {
			$key = $this->apiKeysFacade->removeKey($this->loggedInUser, $id);
			$name = $key->label ?: $key->keyId;
			$this->flashMessage("Key {$name} label removed", 'success');
		}

		$this->redirect('Key:');
	}

	public function handleAdd(SubmitButton $button) {
		/** @var Form $form */
		$form = $button->form;
		$values = $form->getValues();

		$key = $this->apiKeysFacade->saveNewKey($this->loggedInUser, $values['keyId'], $values['vCode'], $values['label']);
		$this->keySession->id = $key->id;

		if ($this->isAjax()) {
			$this->step = 2;
			$this->redrawControl('form');

		} else {
			$this->redirect('this');
		}
	}

}
