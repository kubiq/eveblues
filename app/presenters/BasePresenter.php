<?php

namespace App\Presenters;

use EveBlues\Model\User\User;
use EveBlues\Model\User\UsersRepository;
use Nette;


/**
 * Base presenter for all application presenters.
 */
abstract class BasePresenter extends Nette\Application\UI\Presenter {

	/** @var UsersRepository @inject */
	public $usersRepository;
	/** @var User */
	public $loggedInUser;

	public function startup() {
		parent::startup();

		if ($this->user->isLoggedIn()) {
			$this->loggedInUser = $this->usersRepository->getById($this->user->getId());

			if (!$this->loggedInUser) {
				$this->user->logout(true);
			}


		}
	}

	public function beforeRender() {
		parent::beforeRender();

		$this->template->analytics = $this->context->getParameters()['analytics'];
		$this->template->loggedInUser = $this->loggedInUser;
	}


	/**
	 * @param string $name
	 * @return \Nette\Application\UI\Control
	 */
	protected function createComponent($name) {

		$class = "\\EveBlues\\Controls\\" . ucfirst($name);
		$component = $this->context->getByType($class);
		if ($component) {
			return $component;
		}

		return null;
	}
}
