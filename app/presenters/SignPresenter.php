<?php

namespace App\Presenters;

use Nette;


class SignPresenter extends BasePresenter {

	public function actionIn() {
		$this->user->login();

		if ($this->user->isLoggedIn()) {
			$this->flashMessage("Welcome");
		}

		$this->redirect('Characters:');
	}

	public function actionFinish() {
		
	}

	public function actionOut() {
		$this->user->logout();
		$this->flashMessage("You have been logged out!", 'success');
		$this->redirect('Homepage:');
	}

}
