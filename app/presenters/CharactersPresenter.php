<?php

namespace App\Presenters;

use EveBlues\Auth\EveSSOFactory;
use EveBlues\Crest\Crest;
use EveBlues\Model\Alliance\AlliancesFacade;
use EveBlues\Model\Character\CharactersFacade;
use EveBlues\Model\Character\CharactersRepository;
use EveBlues\XmlApi\XmlApi;
use Nette\Application\UI\Form;

/**
 * Class StandingsPresenter
 * @package App\Presenters
 * @User
 */
class CharactersPresenter extends BasePresenter {

	/** @var EveSSOFactory @inject */
	public $eveSSOFactory;
	/** @var CharactersRepository @inject */
	public $charactersRepository;
	/** @var AlliancesFacade @inject */
	public $allianceFacade;
	/** @var CharactersFacade @inject */
	public $charactersFacade;
	/** @var Crest @inject */
	public $crest;
	/** @var XmlApi @inject */
	public $xmlApi;


	public function startup() {
		parent::startup();
		if (!$this->user->isLoggedIn()) {
			$this->flashMessage('You have to be logged in.');
			$this->redirect('Homepage:');
		}
	}

	public function renderDefault() {

		\Tracy\Debugger::barDump($this->xmlApi->getCharacter(2023798312));

		$this->template->characters = $this->charactersRepository->findWithToken($this->loggedInUser);
	}

	public function actionAdd() {
		$_SESSION['addCharacter'] = $this->user->getId();
		$this->user->login(['add' => $this->user->getId()]);
	}

	/**
	 * @param Form $form
	 * @param array $values
	 */
	public function add(Form $form, array $values) {
		$provider = $this->providerFactory->createNew();

		$request = $provider->getAuthenticatedRequest(
			'GET',
			"https://crest-tq.eveonline.com/characters/2023798312/contacts/",
			$this->loggedInUser->token
		);
		$response = $provider->getResponse($request);
	}

}
