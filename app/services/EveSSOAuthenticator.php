<?php

namespace EveBlues\Auth;

use EveBlues\Model\AccessToken\AccessTokensFacade;
use EveBlues\Model\Character\CharactersFacade;
use EveBlues\Model\Character\CharactersRepository;
use EveBlues\Model\User\UsersFacade;
use EveBlues\Model\User\UsersRepository;
use EveBlues\XmlApi\XmlApi;
use Evelabs\OAuth2\Client\Provider\EveOnline;
use Evelabs\OAuth2\Client\Provider\EveOnlineResourceOwner;
use Nette\Security\AuthenticationException;
use Nette\Security\IAuthenticator;
use Nette\Security\Identity;
use Nette\Security\IIdentity;

class EveSSOAuthenticator implements IAuthenticator {

	/** @var EveSSOFactory */
	public $eveSSOFactory;
	/** @var AccessTokensFacade */
	private $facade;
	/** @var CharactersRepository */
	private $charactersRepository;
	/** @var UsersRepository */
	private $usersRepository;
	/** @var CharactersFacade */
	private $charactersFacade;

	public function __construct(
		CharactersRepository $charactersRepository,
		UsersRepository $usersRepository,
		EveSSOFactory $eveSSOFactory,
		AccessTokensFacade $facade,
		CharactersFacade $charactersFacade
	) {
		$this->eveSSOFactory = $eveSSOFactory;
		$this->facade = $facade;
		$this->charactersRepository = $charactersRepository;
		$this->usersRepository = $usersRepository;
		$this->charactersFacade = $charactersFacade;
	}

	/**
	 * Performs an authentication against e.g. database.
	 * and returns IIdentity on success or throws AuthenticationException
	 * @return IIdentity
	 * @throws AuthenticationException
	 */
	public function authenticate(array $credentials) {
		$provider = $this->eveSSOFactory->createNew();

		if (!isset($_GET['code'])) {

			// If we don't have an authorization code then get one
			$this->facade->getAuthCode();

			// Check given state against previously stored one to mitigate CSRF attack
		} elseif (empty($_GET['state']) || (!isset($_SESSION['oauth2state'])) || ($_GET['state'] !== $_SESSION['oauth2state'])) {

			unset($_SESSION['oauth2state']);
			throw new AuthenticationException('Invalid state');

		} else {

			// Try to get an access token (using the authorization code grant)
			$token = $this->facade->getToken($_GET['code']);

			// Optional: Now you have a token you can look up a users profile data

			// We got an access token, let's now get the user's details
			/** @var EveOnlineResourceOwner $characterData */
			$characterData = $provider->getResourceOwner($token);

			// Use these details to create a new profile

			if (!isset($_SESSION['addCharacter'])) {
				$user = $this->usersRepository->getByCharacterId($characterData->getCharacterID());

				if (!$user) {
					$user = $this->usersRepository->createNew($characterData->getCharacterName());
				}

			} else {
				$user = $this->usersRepository->getById($_SESSION['addCharacter']);
				unset($_SESSION['addCharacter']);
			}

			$character = $this->charactersFacade->createNew($user, $characterData->getCharacterID(), $token);
			$this->usersRepository->addCharacter($user, $character);

			return new Identity($user->id, null, ['name' => $user->name]);
		}
	}
}
