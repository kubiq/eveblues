<?php

namespace EveBlues\XmlApi;

use EveBlues\Api\PhealFactory;
use EveBlues\Model\ApiKey\ApiKey;
use EveBlues\Model\Character\CharactersRepository;
use Kdyby\Monolog\Logger;

class XmlApi {

	/** @var CharactersRepository */
	private $charactersRepository;
	/** @var Logger */
	private $logger;
	/** @var PhealFactory */
	private $phealFactory;

	public function __construct(
		PhealFactory $phealFactory,
		CharactersRepository $charactersRepository,
		Logger $logger
	) {
		$this->charactersRepository = $charactersRepository;
		$this->logger = $logger->channel('xmlApi');
		$this->pheal = $phealFactory->createNew(null, null, PhealFactory::SCOPE_EVE);
		$this->phealFactory = $phealFactory;
	}

	/**
	 * @param int[] $characterIds
	 * @return array
	 */
	public function getCharacters(array $characterIds) {
		$count = count($characterIds);
		if ($count) {
			$this->logger->addInfo("Getting affiliation for {$count} characters");
			$ids = implode(',', $characterIds);
			$this->pheal->scope = PhealFactory::SCOPE_EVE;
			return $this->pheal->CharacterAffiliation(['ids' => $ids])->toArray()['result']['characters'];
		}
		return [];
	}

	public function getCharacter(int $characterId) {
		return $this->getCharacters([$characterId])[0];
	}

	public function getContactList(ApiKey $apiKey, int $characterId) {
		$this->logger->addInfo("Getting contact list for {$characterId}", ['apiKeyId' => $apiKey->id]);
		$pheal = $this->phealFactory->createNewFromKey($apiKey, PhealFactory::SCOPE_CHARACTER);
		return $pheal->ContactList(['characterID' => $characterId])->toArray()['result'];
	}

	public function getAlliances() {
		$this->logger->addInfo("Getting alliances list");
		$this->pheal->scope = PhealFactory::SCOPE_EVE;
		return $this->pheal->AllianceList()->toArray()['result']['alliances'];
	}

}
