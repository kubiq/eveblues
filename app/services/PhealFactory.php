<?php

namespace EveBlues\Api;

use EveBlues\Model\ApiKey\ApiKey;
use Kdyby\Monolog\Logger;
use Pheal\Cache\HashedNameFileStorage;
use Pheal\Core\Config;
use Pheal\Pheal;

class PhealFactory {

	const SCOPE_ACCOUNT = 'account';
	const SCOPE_CHARACTER = 'char';
	const SCOPE_EVE = 'eve';
	/**
	 * @var Logger
	 */
	private $logger;

	public function __construct(
		Logger $logger
	) {
		$this->logger = $logger->channel('pheal');

		Config::getInstance()->cache = new HashedNameFileStorage(TMP_DIR . '/pheal/');
//		Config::getInstance()->log = $this->logger->;
	}

	public function createNew($keyId, $vCode, string $scope) {
		return new Pheal($keyId, $vCode, $scope);
	}

	public function createNewFromKey(ApiKey $apiKey, string $scope) {
		return $this->createNew($apiKey->keyId, $apiKey->vCode, $scope);
	}
}
