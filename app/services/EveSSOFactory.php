<?php

namespace EveBlues\Auth;

use Evelabs\OAuth2\Client\Provider\EveOnline;

class EveSSOFactory {

	private $clientId;
	private $clientSecret;
	private $redirectUri;

	/**
	 * EveSSOFactory constructor.
	 * @param string $clientId
	 * @param string $clientSecret
	 * @param string $redirectUri
	 */
	public function __construct(string $clientId, string $clientSecret, string $redirectUri) {
		$this->clientId = $clientId;
		$this->clientSecret = $clientSecret;
		$this->redirectUri = $redirectUri;
	}

	public function createNew() :EveOnline {
		return new EveOnline([
			'clientId' => $this->clientId,
			'clientSecret' => $this->clientSecret,
			'redirectUri' => $this->redirectUri
		]);
	}

}
