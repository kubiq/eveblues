<?php

namespace EveBlues\Crest;

use EveBlues\Auth\EveSSOFactory;
use EveBlues\Model\Character\Character;
use EveBlues\Model\Character\CharactersRepository;
use EveBlues\Model\Contact\Contact;
use Evelabs\OAuth2\Client\Provider\EveOnline;
use Kdyby\Monolog\Logger;
use League\OAuth2\Client\Provider\Exception\IdentityProviderException;
use Nette\InvalidStateException;

class Crest {

	/** @var EveOnline */
	private $provider;
	/** @var CharactersRepository */
	private $charactersRepository;
	/** @var string */
	private $baseUrl = 'https://crest-tq.eveonline.com';
	/** @var Logger */
	private $logger;

	public function __construct(
		EveSSOFactory $eveSSOFactory,
		CharactersRepository $charactersRepository,
		Logger $logger
	) {
		$this->provider = $eveSSOFactory->createNew();
		$this->charactersRepository = $charactersRepository;
		$this->logger = $logger->channel('crest');
	}

//	public function refreshToken(Character $character) {
//		// https://eveonline-third-party-documentation.readthedocs.io/en/latest/sso/refreshtokens.html
//	}


	public function addContact(Character $character, Contact $contact) {


		$data = [
			'body' => json_encode([
				"watched" => false,
				"standing" => $contact->standing,
				"contact" => [
					"href" => "{$this->baseUrl}/characters/{$contact->subjectId}/",
				],
			]),

		];

		$request = $this->provider->getAuthenticatedRequest(
			'POST',
			"{$this->baseUrl}/characters/{$character->id}/contacts/",
			$character->token,
			$data
		);

		try {
			$response = $this->provider->getResponse($request);
		} catch (IdentityProviderException $ex) {
			if ($ex->getCode() !== 201) {
				throw $ex;
			}
		}
	}

	public function getAlliance(int $allianceId) {
		$this->logger->addInfo('Getting alliance info', ['allianceId' => $allianceId]);
		try {
			$request = $this->provider->getRequest(
				'GET',
				"{$this->baseUrl}/alliances/{$allianceId}/"
			);
			$allianceData = $this->provider->getResponse($request);

			return $allianceData;

		} catch (\Exception $ex) {
			throw $ex;
		}
	}

	public function getAlliances() {

		try {
			$data = $this->getAlliancesPage();
			$this->logger->addInfo('Downloading alliance list', ['page' => 1]);
			$alliancesData = $data['items'];

			$next = isset($data['next']);

			while ($next) {
				$data = $this->getNextPage($data);
				$next = isset($data['next']);
				$newData = $data['items'];
				$alliancesData = array_merge($alliancesData, $newData);
			}

			return $alliancesData;

		} catch (\Exception $ex) {
			throw $ex;
		}
	}

	private function getAlliancesPage() {

		$this->logger->addInfo('Getting alliances list');

		$request = $this->provider->getRequest(
			'GET',
			"{$this->baseUrl}/alliances/"
		);
		$res = $this->provider->getResponse($request);
		return $res;
	}


	private function getNextPage($data) {
		$url = $data['next']['href'];
		$this->logger->addInfo('Downloading next page', ['url' => $url]);

		$req = $this->provider->getRequest(
			'GET',
			$url
		);
		return $this->provider->getResponse($req);
	}

}
