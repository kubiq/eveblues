<?php

use EveBlues\Model\Character\CharactersFacade;
use Kdyby\Monolog\Logger;

require __DIR__ . '/../vendor/autoload.php';

const APP_DIR = __DIR__ . '/../app';
const CONFIG_DIR = APP_DIR . '/config';
const LOG_DIR = APP_DIR . '/../log';
const TMP_DIR = APP_DIR . '/../tmp';

$configurator = new Nette\Configurator;

$configurator->setDebugMode(true);
$configurator->enableDebugger(LOG_DIR);
\Tracy\Debugger::$showBar = false;

$configurator->setTempDirectory(TMP_DIR);

$configurator->createRobotLoader()
	->addDirectory(APP_DIR)
	->register();

$configurator->addConfig(CONFIG_DIR . '/config.neon');
$configurator->addConfig(CONFIG_DIR . '/config.local.neon');

$container = $configurator->createContainer();

/** @var CharactersFacade $facade */
$facade = $container->getByType(CharactersFacade::class);

/** @var Logger $logger */
$logger = $container->getByType(Logger::class);
$logger = $logger->channel('cron');

$logger->addInfo('Affiliation cron started');

$facade->fillCharacters();

$logger->addInfo('Affiliation cron ended');
