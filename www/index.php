<?php

// Uncomment this line if you must temporarily take down your site for maintenance.
// require __DIR__ . '/.maintenance.php';

const APP_DIR = __DIR__ . '/../app';
const CONFIG_DIR = APP_DIR . '/config';
const LOG_DIR = APP_DIR . '/../log';
const TMP_DIR = APP_DIR . '/../tmp';

$container = require __DIR__ . '/../app/bootstrap.php';

$container->getByType(Nette\Application\Application::class)->run();
