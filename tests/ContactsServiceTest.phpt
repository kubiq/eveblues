<?php

namespace Test;

use EveBlues\Model\Character\Character;
use EveBlues\Model\Contact\CharacterContact;
use EveBlues\Model\Contact\ContactsService;
use EveBlues\Model\Orm;
use Nette;
use Tester;
use Tester\Assert;

$container = require __DIR__ . '/bootstrap.php';

class ContactServiceTest extends Tester\TestCase {

	/** @var ContactsService */
	private $service;

	public function __construct(Nette\DI\Container $container) {
		$this->service = $container->getByType(ContactsService::class);
		$container->getByType(Orm::class);
	}

	public function setUp() {
	}

	/**
	 * @dataProvider getContactsData
	 */
	public function testIndexContacts($contacts, $expected) {

		$index = $this->service->indexContacts($contacts);
		Assert::equal($index, $expected);
	}

	public function testHasParentContact() {

		$contacts = $this->getIndexedContacts();
		$childContact = new CharacterContact();
		$childContact->subjectId = 456;

		Assert::equal($this->service->hasParentContact($contacts, $childContact), true);

		$childContact = new CharacterContact();
		$childContact->subjectId = 10;

		Assert::equal($this->service->hasParentContact($contacts, $childContact), false);
	}

	public function getContactsData() {

		return [
			[
				$this->getContacts(),
				$this->getIndexedContacts()
			]
		];
	}


	public function getContacts() {

		$c1 = new CharacterContact();
		$c1->subjectId = 456;
		$c2 = new CharacterContact();
		$c2->subjectId = 86786;
		$c3 = new CharacterContact();
		$c3->subjectId = 234;
		$c4 = new CharacterContact();
		$c4->subjectId = 7567;
		$c5 = new CharacterContact();
		$c5->subjectId = 68587;

		return [$c1, $c2, $c3, $c4, $c5];
	}

	public function getIndexedContacts() {

		$c = $this->getContacts();
		return [456 => $c[0], 86786 => $c[1], 234 => $c[2], 7567 => $c[3], 68587 => $c[4]];
	}

}


$test = new ContactServiceTest($container);
$test->run();
