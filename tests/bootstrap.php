<?php

require __DIR__ . '/../vendor/autoload.php';
require __DIR__ . '/../vendor/nette/tester/src/bootstrap.php';

Tester\Environment::setup();

$configurator = new Nette\Configurator;

$configurator->setDebugMode(false);
\Tracy\Debugger::$showBar = false;

$configurator->setTempDirectory(__DIR__ . '/../tmp');

$configurator->createRobotLoader()
	->addDirectory(__DIR__ . '/../app')
	->register();

$configurator->addConfig(__DIR__ . '/../app/config/config.neon');
$configurator->addConfig(__DIR__ . '/../app/config/config.local.neon');

return $configurator->createContainer();
