-- Adminer 4.2.4 MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

SET NAMES utf8mb4;

DROP TABLE IF EXISTS `access_tokens`;
CREATE TABLE `access_tokens` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `character_id` bigint(20) NOT NULL,
  `added` datetime NOT NULL,
  `token` varchar(250) NOT NULL,
  `expires_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `character_id` (`character_id`),
  CONSTRAINT `access_tokens_ibfk_2` FOREIGN KEY (`character_id`) REFERENCES `characters` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;


DROP TABLE IF EXISTS `api_keys`;
CREATE TABLE `api_keys` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `owner_id` bigint(20) NOT NULL,
  `added` datetime NOT NULL,
  `label` varchar(50) DEFAULT NULL,
  `keyid` int(11) NOT NULL,
  `vcode` varchar(250) NOT NULL,
  `expires` datetime DEFAULT NULL,
  `type` int(11) NOT NULL,
  `access_mask` int(11) NOT NULL,
  `removed` tinyint(4) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `keyid_owner_id` (`keyid`,`owner_id`),
  KEY `owner_id` (`owner_id`),
  CONSTRAINT `api_keys_ibfk_1` FOREIGN KEY (`owner_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;


DROP TABLE IF EXISTS `api_keys_x_characters`;
CREATE TABLE `api_keys_x_characters` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `api_key_id` int(11) NOT NULL,
  `character_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `api_key_id_character_id` (`api_key_id`,`character_id`),
  KEY `character_id` (`character_id`),
  CONSTRAINT `api_keys_x_characters_ibfk_1` FOREIGN KEY (`api_key_id`) REFERENCES `api_keys` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `api_keys_x_characters_ibfk_2` FOREIGN KEY (`character_id`) REFERENCES `characters` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;


DROP TABLE IF EXISTS `characters`;
CREATE TABLE `characters` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `added` datetime NOT NULL,
  `owner_id` bigint(20) NOT NULL,
  `token` varchar(250) DEFAULT NULL,
  `token_expires` datetime DEFAULT NULL,
  `visible` tinyint(4) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`),
  KEY `owner` (`owner_id`),
  KEY `access_token_id` (`token`(191)),
  CONSTRAINT `characters_ibfk_2` FOREIGN KEY (`owner_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;


DROP TABLE IF EXISTS `contacts`;
CREATE TABLE `contacts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `owner_id` bigint(20) NOT NULL,
  `subject_id` bigint(20) NOT NULL,
  `subject_name` varchar(250) NOT NULL,
  `standing` int(11) NOT NULL,
  `type` int(11) NOT NULL,
  `owner_type` int(11) NOT NULL,
  `label_id` int(11) DEFAULT NULL,
  `in_watchlist` tinyint(4) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `label_id` (`label_id`),
  KEY `owner_id` (`owner_id`),
  CONSTRAINT `contacts_ibfk_3` FOREIGN KEY (`label_id`) REFERENCES `contact_labels` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `contacts_ibfk_4` FOREIGN KEY (`owner_id`) REFERENCES `characters` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;


DROP TABLE IF EXISTS `contact_labels`;
CREATE TABLE `contact_labels` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(250) NOT NULL,
  `owner_id` bigint(20) NOT NULL,
  `label_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `owner_id` (`owner_id`),
  CONSTRAINT `contact_labels_ibfk_2` FOREIGN KEY (`owner_id`) REFERENCES `characters` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;


DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `registered` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;


-- 2016-06-24 06:44:43